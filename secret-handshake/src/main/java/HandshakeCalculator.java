import java.util.List;
import java.util.Vector;

class HandshakeCalculator {

    List < Signal > calculateHandshake(int number) {

        final List < Signal > output = new Vector< Signal >();
        int action = 0, action_incr = 1, end = Signal.values().length;

        if ((number & 16) != 0) {
            action = 3;
            action_incr = -1;
            end = -1;
        }

        for (; action != end; action += action_incr)
            if ((number & (1 << action)) != 0)
                output.add(Signal.values()[action]);

        return output;
    }

}
