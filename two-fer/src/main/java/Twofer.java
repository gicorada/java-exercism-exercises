public class Twofer {
    public String twofer(String name) {
        String text = "One for ";
        if (name == null) text += "you";
        else text += name;
        return text += ", one for me.";
    }
}
