class NeedForSpeed {
    private int speed;
    private int batteryDrain;
    private int driven;
    private int percent;

    public NeedForSpeed(int speed, int batteryDrain) {
        this.speed = speed;
        this.batteryDrain = batteryDrain;

        driven = 0;
        percent = 100;
    }

    public boolean batteryDrained() {
        return percent < batteryDrain;
    }

    public int distanceDriven() {
        return driven;
    }

    public void drive() {
        if(!batteryDrained()) {
            driven += speed;
            percent -= batteryDrain;
        }
    }

    public static NeedForSpeed nitro() {
        return new NeedForSpeed(50, 4);
    }
}

class RaceTrack {
    private int distance;

    public RaceTrack(int distance) {
        this.distance = distance;
    }

    public boolean tryFinishTrack(NeedForSpeed car) {
        while(!car.batteryDrained()) {
            car.drive();
        }
        return car.distanceDriven() >= distance;
    }
}
