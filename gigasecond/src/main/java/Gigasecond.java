import java.time.LocalDate;
import java.time.LocalDateTime;

public class Gigasecond {
    private long seconds = 1000000000;
    LocalDateTime afterGiga;
    public Gigasecond(LocalDate moment) {
        this.afterGiga = moment.atStartOfDay();
    }

    public Gigasecond(LocalDateTime moment) {
        this.afterGiga = moment;
    }

    public LocalDateTime getDateTime() {
        return afterGiga.plusSeconds(seconds);
    }
}
