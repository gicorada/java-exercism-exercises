
class BirdWatcher {
    private final int[] birdsPerDay;

    public BirdWatcher(int[] birdsPerDay) {
        this.birdsPerDay = birdsPerDay.clone();
    }

    public int[] getLastWeek() {
        return birdsPerDay;
    }

    public int getToday() {
        return birdsPerDay[birdsPerDay.length - 1];
    }

    public void incrementTodaysCount() {
        birdsPerDay[birdsPerDay.length - 1]++;
    }

    public boolean hasDayWithoutBirds() {
        for(int dailyVisits : birdsPerDay) {
            if(dailyVisits == 0) return true;
        }
        return false;
    }

    public int getCountForFirstDays(int numberOfDays) {
        int visits = 0;
        int days = Math.min(numberOfDays, 7);
        for(int i = 0; i < days; i++) {
            visits += birdsPerDay[i];
        }

        return visits;
    }

    public int getBusyDays() {
        int busyDays = 0;
        for(int i = 0; i < birdsPerDay.length; i++) {
            if(birdsPerDay[i] >= 5) busyDays++;
        }

        return busyDays;
    }
}
