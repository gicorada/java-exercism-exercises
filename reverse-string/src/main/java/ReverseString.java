import java.util.Arrays;
import java.util.Collections;
import java.util.List;

class ReverseString {

    String reverse(String inputString) {
        StringBuilder res = new StringBuilder();
        for(int i = inputString.length() - 1; i >= 0; i--) {
            res.append(inputString.charAt(i));
        }

        return res.toString();
    }
  
}
