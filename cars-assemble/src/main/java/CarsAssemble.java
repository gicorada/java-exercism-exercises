public class CarsAssemble {

    public CarsAssemble() {

    }

    public static final double CARS_PER_HOUR = 221;

    public double productionRatePerHour(int speed) {
        if (speed == 0) return 0;
        double produced = CARS_PER_HOUR;
        if(speed >= 1 && speed <= 4) {
            produced = produced * speed * 100 / 100;
        } else if(speed >= 5 && speed <= 8) {
            produced = produced * speed * 90 / 100;
        } else if(speed == 9) {
            produced = produced * speed * 80 / 100;
        } else if(speed == 10) {
            produced = produced * speed * 77 / 100;
        }

        return produced;
    }

    public int workingItemsPerMinute(int speed) {
        return (int)productionRatePerHour(speed) / 60;
    }

    public static void main(String[] args) {
        CarsAssemble car = new CarsAssemble();

        System.out.println(car.productionRatePerHour(6));

    }
}
