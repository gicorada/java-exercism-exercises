public class ElonsToyCar {
    private int meters;
    private int percentage;

    public ElonsToyCar() {
        meters = 0;
        percentage = 100;
    }
    public static ElonsToyCar buy() {
        return new ElonsToyCar();
    }

    public String distanceDisplay() {
        return "Driven " + meters + " meters";
    }

    public String batteryDisplay() {
        if(percentage <= 0) return "Battery empty";
        return "Battery at " + percentage + "%";
    }

    public void drive() {
        if(percentage <=0) return;
        meters += 20;
        percentage--;
    }
}
