public class Lasagna {
    private static final int TIME = 40;
    private static final int LAYER_TIME = 2;

    // TODO: define the 'expectedMinutesInOven()' method
    public int expectedMinutesInOven() {
        return TIME;
    }

    // TODO: define the 'remainingMinutesInOven()' method

    public int remainingMinutesInOven(int timePast) {
        return Math.max(0, TIME-timePast);
    }

    // TODO: define the 'preparationTimeInMinutes()' method

    public int preparationTimeInMinutes(int layers) {
        return Math.max(0, layers*LAYER_TIME);
    }

    // TODO: define the 'totalTimeInMinutes()' method

    public int totalTimeInMinutes(int layers, int timeOven) {
        return preparationTimeInMinutes(layers) + timeOven;
    }
}
