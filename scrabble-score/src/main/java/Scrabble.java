class Scrabble {

    private String word;

    public Scrabble(String word) {
        this.word = word.toLowerCase();
    }

    public int getScore() {
        int points = 0;
        for(int i = 0; i < word.length(); i++) {
            switch (word.charAt(i)) {
                case 'a':
                case 'e':
                case 'i':
                case 'o':
                case 'u':
                case 'l':
                case 'n':
                case 'r':
                case 's':
                case 't':
                    points += 1;
                    break;

                case 'd':
                case 'g':
                    points += 2;
                    break;

                case 'b':
                case 'c':
                case 'm':
                case 'p':
                    points += 3;
                    break;

                case 'f':
                case 'h':
                case 'v':
                case 'w':
                case 'y':
                    points += 4;
                    break;

                case 'k':
                    points += 5;
                    break;

                case 'j':
                case 'x':
                    points += 8;
                    break;

                case 'q':
                case 'z':
                    points += 10;

                default:

            }
        }

        return points;
    }

}
